use crate::{
    bounds::*,
    node_generics::{quadrant_identification, QuadrantSubNode},
};
use bevy::math::{vec3, Vec3};

/*pub enum QuadrantSubNode {
    NxNyNz,
    NxNyPz,
    NxPyNz,
    NxPyPz,
    PxNyNz,
    PxNyPz,

    PxPyNz,

    PxPyPz,
} */
#[test]
fn nx_ny_nz() {
    let center = vec3(5., 5., 5.);
    let size = vec3(5., 5., 5.);
    let quadrant_point = vec3(2.5, 2.5, 2.5);

    let result = test_quadrant(center, size, quadrant_point);
    assert_eq!(result, Some(QuadrantSubNode::NxNyNz))
}

#[test]
fn nx_ny_pz() {
    let center = vec3(5., 5., 5.);
    let size = vec3(5., 5., 5.);
    let quadrant_point = vec3(2.5, 2.5, 7.5);

    let result = test_quadrant(center, size, quadrant_point);
    assert_eq!(result, Some(QuadrantSubNode::NxNyPz))
}

#[test]
fn nx_py_nz() {
    let center = vec3(5., 5., 5.);
    let size = vec3(5., 5., 5.);
    let quadrant_point = vec3(2.5, 7.5, 2.5);

    let result = test_quadrant(center, size, quadrant_point);
    assert_eq!(result, Some(QuadrantSubNode::NxPyNz))
}

#[test]
fn nx_py_pz() {
    let center = vec3(5., 5., 5.);
    let size = vec3(5., 5., 5.);
    let quadrant_point = vec3(2.5, 7.5, 7.5);

    let result = test_quadrant(center, size, quadrant_point);
    assert_eq!(result, Some(QuadrantSubNode::NxPyPz))
}

#[test]
fn px_ny_nz() {
    let center = vec3(5., 5., 5.);
    let size = vec3(5., 5., 5.);
    let quadrant_point = vec3(7.5, 2.5, 2.5);

    let result = test_quadrant(center, size, quadrant_point);
    assert_eq!(result, Some(QuadrantSubNode::PxNyNz))
}

#[test]
fn px_ny_pz() {
    let center = vec3(5., 5., 5.);
    let size = vec3(5., 5., 5.);
    let quadrant_point = vec3(7.5, 2.5, 7.5);

    let result = test_quadrant(center, size, quadrant_point);
    assert_eq!(result, Some(QuadrantSubNode::PxNyPz))
}

#[test]
fn px_py_nz() {
    let center = vec3(5., 5., 5.);
    let size = vec3(5., 5., 5.);
    let quadrant_point = vec3(7.5, 7.5, 2.5);

    let result = test_quadrant(center, size, quadrant_point);
    assert_eq!(result, Some(QuadrantSubNode::PxPyNz))
}

#[test]
fn px_py_pz() {
    let center = vec3(-5., -5., -5.);
    let size = vec3(5., 5., 5.);
    let quadrant_point = vec3(-2.5, -2.5, -2.5);

    let result = test_quadrant(center, size, quadrant_point);
    assert_eq!(result, Some(QuadrantSubNode::PxPyPz))
}

#[test]
fn center_point_return_none() {
    let center = vec3(5., 5., 5.);
    let size = vec3(5., 5., 5.);

    let result = test_quadrant(center, size, center);
    assert_eq!(result, None)
}

#[test]
fn out_of_bounds_quadrant_test() {
    let center = vec3(5., 5., 5.);
    let size = vec3(5., 5., 5.);
    let quadrant_point = vec3(0., 0., 0.);

    let result = test_quadrant(center, size, quadrant_point);
    assert_eq!(result, None)
}

#[test]
fn bounds_intersection_tests() {
    let b1 = BoundsCenteredIrregular::createCentered(vec3(5., 5., 5.), vec3(5., 5., 5.));
    let b2 = BoundsCenteredIrregular::createCentered(vec3(5., 5., 5.), vec3(5., 5., 5.));

    assert!(b1.intersects(&b2))
}

fn test_quadrant(center_point: Vec3, size: Vec3, quadrant_point: Vec3) -> Option<QuadrantSubNode> {
    let bounds = BoundsCenteredIrregular::createCentered(center_point, size);
    let result = quadrant_identification(bounds, quadrant_point);
    return result;
}
