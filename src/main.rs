mod bounds;
mod node_generics;

use crate::bounds::*;
use crate::node_generics::*;
use bevy::math::vec3;
use bevy::prelude::*;
#[cfg(test)]
mod tests;
mod node_implentations;

fn hello_world() {
    println!("Hello bevy!")
}

fn main() {
    hello_world();
    //App::new().add_system(hello_world).run();
}
