use bevy::math::f32::Vec3;

pub trait VoxelModel {}
#[derive(Copy, Clone)]
pub struct BoundsCenteredIrregular {
    pub center: Vec3,
    pub size: Vec3,
}

impl BoundsCenteredIrregular {
    pub fn createCentered(center_init: Vec3, size_init: Vec3) -> BoundsCenteredIrregular {
        BoundsCenteredIrregular {
            center: center_init,
            size: size_init,
        }
    }
}

impl Bounds for BoundsCenteredIrregular {
    fn x_min(&self) -> f32 {
        self.center.x - self.size.x / 2.
    }
    fn y_min(&self) -> f32 {
        self.center.y - self.size.y / 2.
    }
    fn z_min(&self) -> f32 {
        self.center.z - self.size.z / 2.
    }

    fn x_max(&self) -> f32 {
        self.center.x + self.size.x / 2.
    }
    fn y_max(&self) -> f32 {
        self.center.y + self.size.y / 2.
    }
    fn z_max(&self) -> f32 {
        self.center.z + self.size.z / 2.
    }

    fn x_half(&self) -> f32 {
        self.size.x / 2.
    }
    fn y_half(&self) -> f32 {
        self.size.y / 2.
    }
    fn z_half(&self) -> f32 {
        self.size.z / 2.
    }

    fn x_mid(&self) -> f32 {
        self.center.x
    }
    fn y_mid(&self) -> f32 {
        self.center.y
    }
    fn z_mid(&self) -> f32 {
        self.center.z
    }
}

pub trait Bounds {
    fn x_min(&self) -> f32;
    fn y_min(&self) -> f32;
    fn z_min(&self) -> f32;

    fn x_max(&self) -> f32;
    fn y_max(&self) -> f32;
    fn z_max(&self) -> f32;

    fn x_half(&self) -> f32;
    fn y_half(&self) -> f32;
    fn z_half(&self) -> f32;

    fn x_mid(&self) -> f32;
    fn y_mid(&self) -> f32;
    fn z_mid(&self) -> f32;

    fn intersects<T: Bounds>(&self, other: &T) -> bool {
        let dx = self.x_mid() - other.x_mid();
        let px = (self.x_half() + other.x_half()) - dx.abs();

        if px <= 0. {
            return false;
        }

        let dy = self.y_mid() - other.y_mid();
        let py = (self.y_half() + other.y_half()) - dy.abs();

        if py <= 0. {
            return false;
        }

        let dz = self.z_mid() - other.z_mid();
        let pz = (self.z_half() + other.z_half()) - dz.abs();

        if pz <= 0. {
            return false;
        }

        return true;
    }

    fn contains(&self, point: Vec3) -> bool {
        point.x >= self.x_min()
            && point.x <= self.x_max()
            && point.y >= self.y_min()
            && point.y <= self.y_max()
            && point.z >= self.z_min()
            && point.z <= self.z_max()
    }
}
