use crate::bounds::*;
use bevy::math::f32::Vec3;
use bevy::math::vec3;

#[derive(PartialEq, Eq, Debug)]
pub enum QuadrantSubNode {
    NxNyNz,
    NxNyPz,
    NxPyNz,
    NxPyPz,
    PxNyNz,
    PxNyPz,
    PxPyNz,
    PxPyPz,
}

impl QuadrantSubNode {
    pub fn to_string(self) -> String {
        return match self {
            QuadrantSubNode::NxNyNz => String::from("NxNyNz"),
            QuadrantSubNode::NxNyPz => String::from("NxNyPz"),
            QuadrantSubNode::NxPyNz => String::from("NxPyNz"),
            QuadrantSubNode::NxPyPz => String::from("NxPyPz"),
            QuadrantSubNode::PxNyNz => String::from("PxNyNz"),
            QuadrantSubNode::PxNyPz => String::from("PxNyPz"),
            QuadrantSubNode::PxPyNz => String::from("PxPyNz"),
            QuadrantSubNode::PxPyPz => String::from("PxPyPz"),
        };
    }
    pub fn from_i32(value: i32) -> Option<QuadrantSubNode> {
        match value {
            0 => Some(QuadrantSubNode::NxNyNz),
            1 => Some(QuadrantSubNode::NxNyPz),
            2 => Some(QuadrantSubNode::NxPyNz),
            3 => Some(QuadrantSubNode::NxPyPz),
            4 => Some(QuadrantSubNode::PxNyNz),
            5 => Some(QuadrantSubNode::PxNyPz),
            6 => Some(QuadrantSubNode::PxPyNz),
            7 => Some(QuadrantSubNode::PxPyPz),
            _ => None,
        }
    }

    pub fn to_vec3(element: QuadrantSubNode) -> Vec3 {
        match element {
            QuadrantSubNode::NxNyNz => vec3(-1., -1., -1.),
            QuadrantSubNode::NxNyPz => vec3(-1., -1., 1.),
            QuadrantSubNode::NxPyNz => vec3(-1., 1., -1.),
            QuadrantSubNode::NxPyPz => vec3(-1., 1., 1.),
            QuadrantSubNode::PxNyNz => vec3(1., -1., -1.),
            QuadrantSubNode::PxNyPz => vec3(1., -1., 1.),
            QuadrantSubNode::PxPyNz => vec3(1., 1., -1.),
            QuadrantSubNode::PxPyPz => vec3(1., 1., 1.),
        }
    }
}

pub fn quadrant_identification<T: Bounds>(bounds: T, point: Vec3) -> Option<QuadrantSubNode> {
    if (point.x == bounds.x_mid()) || (point.y == bounds.y_mid()) || (point.z == bounds.z_mid()) {
        return None;
    }
    if !bounds.contains(point) {
        return None;
    }

    let mut result = 0;

    if point.x >= bounds.x_mid() {
        result += 4;
    }

    if point.y >= bounds.y_mid() {
        result += 2;
    }

    if point.z >= bounds.z_mid() {
        result += 1;
    }

    return QuadrantSubNode::from_i32(result);
}


pub trait VoxelTree {
    fn set_node(&mut self, position: &dyn Bounds,
                renderable: &dyn Renderable);
    fn get_node(&self, position: Vec3);
    fn bounds(&self) -> &dyn Bounds;
}

pub trait Renderable : Copy + Clone{}

pub trait VoxelNode {
    fn children(&mut self) -> [&mut dyn VoxelNode; 8];
    fn bounds(&self) -> &dyn Bounds;
    fn renderable(&self) -> &dyn Renderable;
    fn set_renderable(&mut self, renderable: &dyn Renderable);
}

struct VoxelModel {
    root: Box<dyn VoxelNode>,
}

impl VoxelTree for VoxelModel {
    fn set_node(&mut self, position: &dyn Bounds, renderable: &dyn Renderable) {
        todo!()
    }

    fn get_node(&self, position: Vec3) {
        todo!()
    }

    fn bounds(&self) -> &dyn Bounds {
        //self.root.bounds()
        todo!()
    }
}